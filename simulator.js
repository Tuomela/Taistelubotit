class simulator{
	
	constructor(){
		this.bots = [];
		this.bots["player0"] = new randomBot("player0");
		this.bots["player1"] = new rationalBot("player1");
		this.bots["player2"] = new randomBot("player2");
		this.initializeGame();
	}
	
	runGame(){
		console.log("running game with " + this.game.players.length + " players");
		var endIndex = 0;
		for(var step =0; step<100000; step++){
			var situation = this.game.getSituation();
			for(var n =0; n<this.game.players.length; n++){
				var bot = this.bots[this.game.players[n].name];
				if(bot != null){
					try{
						var commandStr = bot.getCommandStr(situation);
						this.game.addCommand(commandStr);
					}catch(err){
						console.log(err);
					}
				}
			}
			
			if(this.game.players.length > 1){
				endIndex = step;
			}
			else if(step - endIndex > 10){
				break;
			}
		}
		console.log("game run completed with " + step + " steps!");
	}
	
	initializeGame(){
		var botNames = new Array();
		for(var botName in this.bots){
			botNames.push(botName);
		}
		this.game = new gameController(botNames, 800, 600);
		console.log("initialized game with " + botNames.length + " bots.");
	}
	
}