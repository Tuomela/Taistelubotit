class ship{
	constructor(name){
		this.energy = 10;
		this.x = 0.0;
		this.y = 0.0;
		this.xspeed = 0.0
		this.yspeed = 0.0;
		this.rot = 0.0;
		this.name = name;
		this.load = 0;
	}
};

class bullet{
	constructor(){
		this.x = 0.0;
		this.y = 0.0;
		this.xspeed = 0.0
		this.yspeed = 0.0;
	}
}

bullet.speed = 3;
ship.power = 0.2;
ship.rotatySpeed = 0.2;
ship.loadTime = 50;

class command{
	//parametrit ovat kaasuttaminen, kääntyminen ja ampuminen
	constructor(t, r, a){
		this.rot = r;
		this.thrust = t;
		this.ammo = a;
	}
	toString(){
		return this.thrust + "," + this.rot + "," + this.ammo;
	}
};

class gameController{

	//parametrit ovat bottien nimet taulukkona, alueen leveys ja alueen korkeus
	constructor(names, w, h){
		this.botNames = names;
		this.players = new Array();
		this.tempCommands = new Array();
		this.areaW = w;
		this.areaH = h;
		this.initialize(names);
		this.allCommandStrs = new Array();
		this.bullets = new Array();
	}
	
	//Luo pelaajat annetuilla bottien nimillä. Tallentaa samalla pelaajien aloitussijainnit taulukkoon.
	initialize(names){
		this.botInitializePositions = new Array();
		for(var i = 0; i<names.length; i++){
			this.players.push(new ship(names[i]));
			this.players[i].x = Math.random() * this.areaW;
			this.players[i].y = Math.random() * this.areaH;
			this.botInitializePositions.push({x: this.players[i].x, y:this.players[i].y});
		}
	}
	
	//Tällä funktiolla voi explisiittisesti määrittää pelaajien sijainnin. Käytetään silloin, kun katsotaan jo kerran pelattu peli.
	initBotPositions(positions){
		for(var n =0; n<this.players.length; n++){
			this.players[n].x = positions[n].x;
			this.players[n].y = positions[n].y;
		}
	}

	//Tällä funktiolla syötetään botin komennot peliin. Jokainen botti syöttää vuorollaan komennon siinä järjestyksessä, kun ne on peliin alustettu.
    //Kun kaikki ovat syöttäneet komennon, ajetaan uusi kierros "runStep"-funktiolla.
	//Syötetyt komennot tallennetaan allCommandStrs taulukkoon stringinä. Tätä taulukkoa voi sitten käyttää, jos haluaa katsoa pelin gamePlayerilla.
	addCommand(commandStr){
		var c;
		var success = true;
		
		//tarkistetaan, että komento on oikeaa muotoa
		var commandRegexp = /(-)?[0,1],(-)?[0,1],[0,1]/;
		if(commandRegexp.test(commandStr)){
			var splitted = commandStr.split(",");
			c = new command(parseInt(splitted[0]), parseInt(splitted[1]), parseInt(splitted[2]));
		}else{
			//jos komento oli väärää muotoa, syötetään nollakomento.
			c = new command(0,0,0);
			success = false;
		}
		this.tempCommands.push(c);
		this.allCommandStrs.push(c.toString());
		
		if(this.tempCommands.length == this.players.length){
			this.runStep(this.tempCommands);
			this.tempCommands = [];
		}
		
		if(!success)
			throw("Komennon täytyy olla muotoa '0,0,0'. Kaksi ensimmäistä kokonaislukua voi olla väliltä [-1, 1]. Viimeinen numero on joko 0 tai 1.");
	}

	//Tämä funktio simuloi yhden pelin kierroksen
	runStep(commands){
		for(var i=0; i<this.players.length; i++){
			var valid = this.updateShip(this.players[i], commands[i]);
			if(!valid){
				this.players.splice(i, 1);
				i--;
			}
		}
		for(var i=0; i<this.bullets.length; i++){
			var valid = this.updateBullet(this.bullets[i]);
			if(!valid){
				this.bullets.splice(i, 1);
				i--;
			}
		}
	}
	
	//Tällä funktiolla saadaan pelin tilanne json muodossa
	getSituation(){
		var json = {"players" : this.players, "bullets" : this.bullets};
		return JSON.stringify(json);
	}

	updateShip(s, c){
		if(s.energy <= 0)
			return false;
		
		s.xspeed += Math.cos(s.rot)*c.thrust*ship.power;
		s.yspeed += Math.sin(s.rot)*c.thrust*ship.power;
		s.x += s.xspeed;
		s.y += s.yspeed;
		s.rot += c.rot * ship.rotatySpeed;
		
		if(s.x > this.areaW) s.x = this.areaW;
		if(s.y > this.areaH) s.y = this.areaH;
		if(s.x < 0) s.x = 0;
		if(s.y < 0) s.y = 0;
		
		if(c.ammo == 1 && s.load == ship.loadTime)
			this.createBullet(s);
		
		s.xspeed *= 0.98;
		s.yspeed *= 0.98;
		
		if(s.load < ship.loadTime)
			s.load++;
		
		return true;
	}
	
	updateBullet(b){
		b.x += b.xspeed;
		b.y += b.yspeed;
		
		for(var n=0; n<this.players.length; n++){
			if(this.checkBulletHit(b, this.players[n])){
				this.players[n].energy--;
				return false;
			}
		}
		
		if(b.x > this.areaW || b.y > this.areaH || b.x < 0 || b.y < 0){
			return false;
		}else{
			return true;
		}
	}
	
	checkBulletHit(b, s){
		return Math.sqrt(Math.pow(b.x-s.x,2) + Math.pow(b.y - s.y,2))<20;
	}
	
	createBullet(s){
		var b = new bullet();
		var bxspeed = Math.cos(s.rot) * bullet.speed;
		var byspeed = Math.sin(s.rot) * bullet.speed;
		b.xspeed = bxspeed + s.xspeed;
		b.yspeed = byspeed + s.yspeed;
		b.x = s.x + bxspeed * 10;
		b.y = s.y + byspeed * 10;
		this.bullets.push(b);
		s.load = 0;
	}
}