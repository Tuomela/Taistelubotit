class bot{
	constructor(name){
		this.name = name;
		if(this.constructor === bot){
			throw "cannot instantiate abstract class!";
		}
		
		if(this.getCommandStr === undefined){
			throw "bot class must implement function getCommandStr!";
		}
	}
}