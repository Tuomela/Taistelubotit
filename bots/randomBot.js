class randomBot extends bot{
	getBot(players){
		for(var i = 0; i < players.length; i++)
		{
		  if(players[i]["name"] == this.name)
		  {
			return players[i];
		  }
		}
	}
	
	getCommandStr(situation){
		var json = JSON.parse(situation);
		
		var player = this.getBot(json["players"]);
		var x = parseFloat(player["x"]);
		var y = parseFloat(player["y"]);
		var rot = parseFloat(player["rot"]);
		
		var r = Math.random() - 0.5 > 0 ? 1 : -1;
		if(r>1) r = 1;
		if(r<-1) r= -1;
		
		var ammo = Math.random() > 0.99 ? 1 : 0;
		var thrust = Math.random() > 0.5 ? 1 : 0;
		
		return thrust +"," + r + "," + ammo;
	}
}