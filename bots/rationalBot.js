class rationalBot extends bot{	
	getBot(players){
		for(var i = 0; i < players.length; i++)
		{
		  if(players[i]["name"] == this.name)
		  {
			return players[i];
		  }
		}
	}
	
	getEnemy(players, bot){
		for(var i = 0; i < players.length; i++)
		{
		  if(players[i]["name"] != this.name)
		  {
			  return players[i];
		  }
		}
		return null;
	}
	
	getCommandStr(situation){
		var json = JSON.parse(situation);
		
		var player = this.getBot(json["players"]);
		var enemy = this.getEnemy(json["players"], player);
		var x = parseFloat(player.x);
		var y = parseFloat(player.y);
		var rot = parseFloat(player.rot);
		var load = parseInt(player.load);
		
		if(enemy != null){
			var enemyY = parseFloat(enemy.y);
			var enemyX = parseFloat(enemy.x);
			
			var r = Math.atan2(enemyY - y, enemyX - x) - rot;
			if(r>0.1) r = 1;
			else if(r<-0.1) r= -1;
			else r = 0;
			
			var ammo = Math.abs(r) < 0.1 && load >= 10 ? 1 : 0;
			if(ammo == 1) this.loader = 0;
			var thrust = Math.abs(r) < 0.2 &&  load > 5 ? 1 : 0;
			
			return thrust +"," + r + "," + ammo;
		}else{
			return "0,0,0";
		}
	}
}