// Create the canvas
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");

// Background image
var bgReady = false;
var bgImage = new Image();
bgImage.onload = function () {
	bgReady = true;
};
bgImage.src = "images/sky.png";

var bulletImgReady = false;
var bulletImg = new Image();
bulletImg.onload = function(){
	bulletImgReady = true;
};
bulletImg.src = "images/bullet.png";

var colors = ["255,0,0", "0,255,0", "0,0,255", "200, 0, 100", "0,200, 100", "200,150,0", "50, 50, 50", "10, 100, 150"];
var shipColors = [];

var game;
var commands;
var commandId = 0;

// Draw everything
var render = function () {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	
	if (bgReady) {
		ctx.drawImage(bgImage, 0, 0, game.areaW, game.areaH);
	}
	
	for(n=0; n<game.players.length; n++){
		ctx.translate( game.players[n].x, game.players[n].y );
		ctx.rotate( game.players[n].rot );
		drawShipWithColor(shipColors[game.players[n].name]);
		ctx.resetTransform();
	}
	
	if(bulletImgReady){
		for(n=0; n<game.bullets.length; n++){
			ctx.drawImage(bulletImg, game.bullets[n].x - 1, game.bullets[n].y - 1);
		}
	}
}

var drawShipWithColor = function(color){
	ctx.beginPath();
	ctx.moveTo(20, 0);
	ctx.lineTo(-20, 10);
	ctx.lineTo(-20, -10);
	ctx.fillStyle = "rgb(" + color + ")";
	ctx.fill();
}

//add commands until game ends
var update = function () {
	for(var n=0; n<game.players.length; n++){
		if(commands.length > commandId){
			game.addCommand(commands[commandId]);
			commandId++;
		}else{
			console.log("game over!");
		}
	}
}

// The main game loop
var main = function () {
	update();
	render();

	requestAnimationFrame(main);
};

// initialize game player, and gameController with given names and positions. saves given commands to array
var initializeGamePlayer = function (botNames, botPositions, allCommands, w, h) {
	for(var i = 0; i< botNames.length; i++)
		shipColors[botNames[i]] = colors[i%colors.length];
	
	game = new gameController(botNames, w, h);
	game.initBotPositions(botPositions);
	commands = allCommands;
	
	canvas.width = w;
	canvas.height = h;
	document.body.appendChild(canvas);
	
	main();
}