﻿Tässä ois tämmönen kokeilu, jota vois soveltaa tekoälykilpailuun.
HUOM! tämä ei ole valmis peli.
Kiinnostavat tiedostot ovat gameController.js ja gamePlayer.js.

simulator.js on nopea testityökalu, jolla kokeilin omia bottejani, (ks. bots kansio).
simulator.js pitäisi tietysti koodata uudestaan kilpailuun.

gameController tiedosto sisältää Game luokan, joka simuloi pelin.
Joka kierroksella käydään pelissä olevat pelaajat läpi, valitaan nimen perusteella botti, ja lähetetään komento peliin.
Kun peliin on tullut pelaajien verran komentoja, suoritetaan yksi simulaatiokierros.
Lopulta pelissä on enään yksi pelaaja jäljellä, ja voidaan palkita voittanut botti.
Mitään pistelaskua tähän ei ole vielä tehty.

Kun peli on simuloitu, voidaan tallennetut komennot ja bottien nimet syöttää gamePlayeriin,
joka pelaa pelin uudestaan läpi tallennettujen komentojen mukaan. (Ja näyttää visuaalisesti mitä tapahtuu).

Jatkokehitysideoita:
-pitäisikö alusten teleportata alueen toiselle laidalle, kun törmätään reunaan?
-pitäisikö alusten törmätä toisiinsa?
-pitäisikö osumasta saada pisteitä? Nykyinen pelimekaniikka ei tue tätä, mutta olis helposti tehtävissä.